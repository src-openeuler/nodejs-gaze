%{?nodejs_find_provides_and_requires}
%global enable_tests 1
%global commitid dad5345a2876346fe49a68ee6e4db34205680798
Name:                nodejs-gaze
Version:             0.5.2
Release:             3
Summary:             A globbing fs.watch wrapper built from parts of other watch libraries
License:             MIT
URL:                 https://github.com/shama/gaze
Source0:             https://github.com/shama/gaze/archive/refs/tags/v0.5.2.tar.gz
Source1:             https://raw.githubusercontent.com/shama/gaze/%{commitid}/Gruntfile.js
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(async) npm(globule) npm(grunt) npm(grunt-cli) npm(grunt-contrib-nodeunit)
BuildRequires:       npm(nodeunit) npm(rimraf)
%endif

%description
This Node.js module provides a globbing fs.watch wrapper built from the best
parts of other fine watch libraries: speedy data behavior from chokidar, the
API interface from watch, and file globbing using glob.

%prep
%setup -q -n gaze-%{version}
cp -p %{SOURCE1} .
%nodejs_fixdep globule '~0.2'

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/gaze
cp -pr package.json lib/ \
    %{buildroot}%{nodejs_sitelib}/gaze
%nodejs_symlink_deps

%if 0%{?enable_tests}
%check
%nodejs_symlink_deps --check
/usr/bin/grunt nodeunit -v --force
%endif

%files
%doc LICENSE-MIT README.md
%{nodejs_sitelib}/gaze

%changelog
* Wed Nov 23 2022 yaoxin <yaoxin30@h-partners.com> - 0.5.2-3
- Modify invalid Source

* Wed Sep 16 2020 leiju <leiju4@huawei.com> - 0.5.2-2
- Fix requires globule version

* Tue Sep 8 2020 yaokai <yaokai13@huawei.com> - 0.5.2-1
- package init
